﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGCharacter.Model.Classes;
using RPGCharacter.Model;
using System.IO;
using System.Data.SQLite;

namespace UItest
{
    public partial class Form1 : Form
    {
        // Magdeli Holmøy Asplin
        // 8/22/2019

        // Attributes that needs declaration
        static Character yourCharacter;
        static string name;
        static string gender;


        
        public Form1()
        {
            InitializeComponent();

            // Creating options in the combobox
            chooseTypeComboBox.Items.Add("Warrior");
            chooseTypeComboBox.Items.Add("Thief");
            chooseTypeComboBox.Items.Add("Mage");

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {
            // Setting the name to be the input from textbox 1
            name = textBox1.Text;
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {
            // Setting the gender to be the input from textbox 2
            gender = textBox2.Text;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            // Creating the character based on which choice the user made from the combobox

            if (chooseTypeComboBox.SelectedItem.ToString() == "Warrior")
            {
                Warrior warrior = new Warrior();
                yourCharacter = new Character(warrior, name, gender);
            }
            else if (chooseTypeComboBox.SelectedItem.Equals("Thief"))
            {
                Thief thief = new Thief();
                yourCharacter = new Character(thief, name, gender);
            }
            else
            {
                Mage mage = new Mage();
                yourCharacter = new Character(mage, name, gender);
            }

            // Printing out a summary of the character to the screen

            MessageBox.Show(yourCharacter.CharacterInfo());

            // Writing the character's type, name and gender to a textfile. This is done such
            // that it can be used in Task15

            using (StreamWriter writetext = new StreamWriter("CharacterInsertSQL.txt"))
            {
                writetext.WriteLine($"INSERT INTO Characters(ClassType, Name, Gender) VALUES(\"{yourCharacter.Class.ClassName}\", \"{name}\", \"{gender}\");");
            }

            // Adding the character to the characterdatabase

            using(SQLiteConnection mySqliteConn = CreateConnection())
            {
                // Inserting the character
                InsertData(mySqliteConn);

                // Read from the database
                txtDatabaseTable.Text = ReadData(mySqliteConn);

                // Closing the connection
                mySqliteConn.Close();

            }

        }

        #region SQL methods
        // All SQL methods are made here
        static SQLiteConnection CreateConnection()
        {
            // Creates a new database connection
            SQLiteConnection sqLite_conn;

            sqLite_conn = new SQLiteConnection("Data Source = Task15.db; Version = 3; New = True; Compress = True; ");

            // Opens the connection
            try
            {
                sqLite_conn.Open();
            }
            catch (SQLiteException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return sqLite_conn;
        }

        static void InsertData(SQLiteConnection conn)
        {
            // Inserts an item in the database
            SQLiteCommand sqLite_cmd;

            sqLite_cmd = conn.CreateCommand();
            sqLite_cmd.CommandText = ($"INSERT INTO Characters(ClassType, Name, Gender) VALUES(\"{yourCharacter.Class.ClassName}\", \"{name}\", \"{gender}\");");
            sqLite_cmd.ExecuteNonQuery();

        }

        static string ReadData(SQLiteConnection conn)
        {
            // Retrieves the data from the database and formate it into text that can be displayed
            // by a textbox

            SQLiteDataReader sqLite_datareader;
            SQLiteCommand sqLite_cmd;

            sqLite_cmd = conn.CreateCommand();
            sqLite_cmd.CommandText = "SELECT * FROM Characters";

            sqLite_datareader = sqLite_cmd.ExecuteReader();
            string myReader = "";
            while (sqLite_datareader.Read())
            {
                myReader += ($" Character type: {sqLite_datareader.GetString(1)}   Name: {sqLite_datareader.GetString(2)}   Gender: {sqLite_datareader.GetString(3)}\n"); // \n - next line, " " - space
               
            }

            return myReader;

        }

        static void DeleteData(SQLiteConnection conn)
        {
            // Deletes data from the database

            // Inserts an item in the database
            SQLiteCommand sqLite_cmd;

            sqLite_cmd = conn.CreateCommand();
            sqLite_cmd.CommandText = ($"DELETE FROM Characters WHERE Name = \"{name}\";");
            sqLite_cmd.ExecuteNonQuery();

        }

        static void UpdateData(SQLiteConnection conn)
        {
            // Updates the charactertype of a character

            SQLiteCommand sqLite_cmd;

            sqLite_cmd = conn.CreateCommand();
            sqLite_cmd.CommandText = ($"UPDATE Characters SET ClassType = \"{yourCharacter.Class.ClassName}\" WHERE Name = \"{name}\";");
            sqLite_cmd.ExecuteNonQuery();
        }

        static void UpdateDataGender(SQLiteConnection conn)
        {
            // Updates the gender of a character

            SQLiteCommand sqLite_cmd;

            sqLite_cmd = conn.CreateCommand();
            sqLite_cmd.CommandText = ($"UPDATE Characters SET Gender = \"{gender}\" WHERE Name = \"{name}\";");
            sqLite_cmd.ExecuteNonQuery();
        }

        #endregion

        private void BtnPrintDatabasetable_Click(object sender, EventArgs e)
        {
            // Button which only displays the database

            using (SQLiteConnection mySqliteConn = CreateConnection())
            {

                // Read from the database
                txtDatabaseTable.Text = ReadData(mySqliteConn);

                // Closing the connection
                mySqliteConn.Close();

            }
        }

        private void BtnDeleteCharacter_Click(object sender, EventArgs e)
        { 
            // Deleting the character to the characterdatabase

            using (SQLiteConnection mySqliteConn = CreateConnection())
            {
                // Deleting the character
                DeleteData(mySqliteConn);

                // Closing the connection
                mySqliteConn.Close();

            }
        }

        private void BtnUpdateCharacter_Click(object sender, EventArgs e)
        {
            // Choosing new type from combobox

            if (chooseTypeComboBox.SelectedItem.ToString() == "Warrior")
            {
                Warrior warrior = new Warrior();
                yourCharacter = new Character(warrior, name, gender);
            }
            else if (chooseTypeComboBox.SelectedItem.Equals("Thief"))
            {
                Thief thief = new Thief();
                yourCharacter = new Character(thief, name, gender);
            }
            else
            {
                Mage mage = new Mage();
                yourCharacter = new Character(mage, name, gender);
            }

            // Updating the character in the characterbase

            using (SQLiteConnection mySqliteConn = CreateConnection())
            {
                // Updating the charactertype of the character
                UpdateData(mySqliteConn);

                // Closing the connection
                mySqliteConn.Close();

            }
        }

        private void BtnUpdateCharacterGender_Click(object sender, EventArgs e)
        {
            // Updating the character in the database

            using (SQLiteConnection mySqliteConn = CreateConnection())
            {
                // Updating the gender of the character
                UpdateDataGender(mySqliteConn);

                // Closing the connection
                mySqliteConn.Close();

            }
        }
    }

}
