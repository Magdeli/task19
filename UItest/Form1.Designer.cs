﻿namespace UItest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chooseTypeComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDatabaseTable = new System.Windows.Forms.RichTextBox();
            this.btnPrintDatabasetable = new System.Windows.Forms.Button();
            this.Headline = new System.Windows.Forms.Label();
            this.btnDeleteCharacter = new System.Windows.Forms.Button();
            this.btnUpdateCharacter = new System.Windows.Forms.Button();
            this.btnUpdateCharacterGender = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // chooseTypeComboBox
            // 
            this.chooseTypeComboBox.FormattingEnabled = true;
            this.chooseTypeComboBox.Location = new System.Drawing.Point(209, 86);
            this.chooseTypeComboBox.Name = "chooseTypeComboBox";
            this.chooseTypeComboBox.Size = new System.Drawing.Size(121, 28);
            this.chooseTypeComboBox.TabIndex = 0;
            this.chooseTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 86);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Choose your type:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(209, 130);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(121, 26);
            this.textBox1.TabIndex = 3;
            this.textBox1.TextChanged += new System.EventHandler(this.TextBox1_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 172);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gender:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(209, 172);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(121, 26);
            this.textBox2.TabIndex = 5;
            this.textBox2.TextChanged += new System.EventHandler(this.TextBox2_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(78, 221);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 32);
            this.button1.TabIndex = 6;
            this.button1.Text = "Make my character";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // txtDatabaseTable
            // 
            this.txtDatabaseTable.Location = new System.Drawing.Point(351, 79);
            this.txtDatabaseTable.Name = "txtDatabaseTable";
            this.txtDatabaseTable.Size = new System.Drawing.Size(425, 265);
            this.txtDatabaseTable.TabIndex = 7;
            this.txtDatabaseTable.Text = "";
            // 
            // btnPrintDatabasetable
            // 
            this.btnPrintDatabasetable.Location = new System.Drawing.Point(473, 366);
            this.btnPrintDatabasetable.Name = "btnPrintDatabasetable";
            this.btnPrintDatabasetable.Size = new System.Drawing.Size(158, 57);
            this.btnPrintDatabasetable.TabIndex = 8;
            this.btnPrintDatabasetable.Text = "Display all characters";
            this.btnPrintDatabasetable.UseVisualStyleBackColor = true;
            this.btnPrintDatabasetable.Click += new System.EventHandler(this.BtnPrintDatabasetable_Click);
            // 
            // Headline
            // 
            this.Headline.AutoSize = true;
            this.Headline.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Headline.Location = new System.Drawing.Point(12, 9);
            this.Headline.Name = "Headline";
            this.Headline.Size = new System.Drawing.Size(306, 46);
            this.Headline.TabIndex = 9;
            this.Headline.Text = "RPG Character";
            // 
            // btnDeleteCharacter
            // 
            this.btnDeleteCharacter.Location = new System.Drawing.Point(16, 280);
            this.btnDeleteCharacter.Name = "btnDeleteCharacter";
            this.btnDeleteCharacter.Size = new System.Drawing.Size(314, 35);
            this.btnDeleteCharacter.TabIndex = 10;
            this.btnDeleteCharacter.Text = "Delete the character of this name";
            this.btnDeleteCharacter.UseVisualStyleBackColor = true;
            this.btnDeleteCharacter.Click += new System.EventHandler(this.BtnDeleteCharacter_Click);
            // 
            // btnUpdateCharacter
            // 
            this.btnUpdateCharacter.Location = new System.Drawing.Point(16, 321);
            this.btnUpdateCharacter.Name = "btnUpdateCharacter";
            this.btnUpdateCharacter.Size = new System.Drawing.Size(314, 52);
            this.btnUpdateCharacter.TabIndex = 11;
            this.btnUpdateCharacter.Text = "Update the type of the character of this name";
            this.btnUpdateCharacter.UseVisualStyleBackColor = true;
            this.btnUpdateCharacter.Click += new System.EventHandler(this.BtnUpdateCharacter_Click);
            // 
            // btnUpdateCharacterGender
            // 
            this.btnUpdateCharacterGender.Location = new System.Drawing.Point(16, 379);
            this.btnUpdateCharacterGender.Name = "btnUpdateCharacterGender";
            this.btnUpdateCharacterGender.Size = new System.Drawing.Size(314, 59);
            this.btnUpdateCharacterGender.TabIndex = 12;
            this.btnUpdateCharacterGender.Text = "Update the gender of the character of this name";
            this.btnUpdateCharacterGender.UseVisualStyleBackColor = true;
            this.btnUpdateCharacterGender.Click += new System.EventHandler(this.BtnUpdateCharacterGender_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnUpdateCharacterGender);
            this.Controls.Add(this.btnUpdateCharacter);
            this.Controls.Add(this.btnDeleteCharacter);
            this.Controls.Add(this.Headline);
            this.Controls.Add(this.btnPrintDatabasetable);
            this.Controls.Add(this.txtDatabaseTable);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chooseTypeComboBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox chooseTypeComboBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox txtDatabaseTable;
        private System.Windows.Forms.Button btnPrintDatabasetable;
        private System.Windows.Forms.Label Headline;
        private System.Windows.Forms.Button btnDeleteCharacter;
        private System.Windows.Forms.Button btnUpdateCharacter;
        private System.Windows.Forms.Button btnUpdateCharacterGender;
    }
}

