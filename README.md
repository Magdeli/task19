# Task19

Magdeli Holmøy Asplin
8/23/2019

This program updates the RPG UI to store characters in a database and enables the user to save a character in the database, retrieve all characters from this database, delete a character of a certain name and update a character of a certain name.
